import * as React from 'react';
import { Text, View, StyleSheet, Button,TouchableHighlight, Image  } from 'react-native';
import Constants from 'expo-constants';
import * as Speech from 'expo-speech';

export default class App extends React.Component {
  
  speak() {
    var thingToSay = `
   Bonjour, je m'appelle Civi. Spécialiste en Softe Skilze . Et coatch révélateur de compétences. 
    Tu as certainement des talents cachés.
    Je vais t'aider à révéler tes qualités.
    `;
   
    Speech.speak(
      thingToSay, 
      // Le changement de voix ne fonctionne pas , à investir !
      // Et à tester sur IOS & Android
      //{ name: "Anna", voiceURI: "com.apple.ttsbundle.Anna-compact", lang: "fr", localService: true, "default": true }
      { language: "fr" }
      );
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={this.speak}>
        <View style={styles.button}>
        <Image
            style={styles.logo}
            source={require('./images/coatch.png')}
          />
          <Text style={styles.paragraph}>Clique-moi dessus !</Text>
        </View>
      </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    alignItems: 'center',
    //backgroundColor: '#DDDDDD',
    padding: 10,
  },
});