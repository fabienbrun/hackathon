import React from 'react';
import logo from './logo.svg';
import './App.css';
import { ConversationalForm } from 'conversational-form';
import SimpleComponent from './components/SimpleComponent'
import MyForm from './components/MyForm'

function App() {
  return (
    <div >
      <SimpleComponent ></SimpleComponent>
      <MyForm ></MyForm>
  
    </div>
  );
}

export default App;
