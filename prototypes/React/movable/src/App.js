import React from 'react';
import { List, arrayMove } from 'react-movable';
import './App.css';

function App() {
  const [items, setItems] = React.useState([
    'Sens de la pédagogie', 
    'Être rigoureux', 
    'Sens de l’organisation',
    'Être autonome',
    'Être à l’écoute',
    'Avoir l’esprit d’équipe',
    'Sens des responsabilités',
    'Sens de l’initiative',
    'Sens de l’analyse',
    'Curiosité intellectuelle',
    ]
    );

  return (
    <div
    style={{
      marginLeft: '10px',
      marginRight: '10px'
    }}
  >
    <List
      lockVertically
      values={items}
      onChange={({ oldIndex, newIndex }) =>
        setItems(arrayMove(items, oldIndex, newIndex))
      }
      renderList={({ children, props, isDragged }) => (
        <ul
          {...props}
          style={{ padding: 0, cursor: isDragged ? 'grabbing' : undefined }}
        >
          {children}
        </ul>
      )}
      renderItem={({ value, props, isDragged, isSelected, index }) => (
        <li
          {...props}
          style={{
            ...props.style,
            padding: '1.5em',
            margin: '0.5em 0em',
            marginLeft: -index*5+50 +'%',
            listStyleType: 'none',
            cursor: isDragged ? 'grabbing' : 'grab',
            border: '2px solid #CCC',
            boxShadow: '3px 3px #AAA',
            color: '#333',
            borderRadius: '5px',
            fontFamily: 'Arial, "Helvetica Neue", Helvetica, sans-serif',
            backgroundColor: isDragged || isSelected ? '#EEE' : '#FFF'
          }}
        >
          {value}
        </li>
      )}
    />
  </div>
  );
}

export default App;
