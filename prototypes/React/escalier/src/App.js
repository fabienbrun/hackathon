import React from 'react';
import {Button} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import { Grid, Row, Col } from 'react-flexbox-grid';
import './App.css';
import SelectExample from './component/SelectExample'

function App() {
  return (
    <div >
     <Button primary>Primary</Button>
     <Button secondary>Secondary</Button>
     <SelectExample style={{flex:1}}></SelectExample>
     <Grid fluid>
          <Row>
            <Col style={colstyle} xsOffset={11} xs={1}>fvfvfvf</Col>
            <Col style={colstyle} xsOffset={10} xs={2} >fvfv</Col>
            <Col style={colstyle} xsOffset={9} xs={3} >fvfvf</Col>
            <Col style={colstyle} xsOffset={8} xs={4} >vfvfv</Col>
            <Col style={colstyle} xsOffset={7} xs={5} >vfvfv</Col>
            <Col style={colstyle} xsOffset={6} xs={6}>fvvf</Col>
            <Col style={colstyle} xsOffset={5} xs={7} >fvfv</Col>
            <Col style={colstyle} xsOffset={4} xs={8} >fvf</Col>
            <Col style={colstyle} xsOffset={3} xs={9} >fvfv</Col>
            <Col style={colstyle} xsOffset={2} xs={10}>fvv</Col>
            <Col style={colstyle} xsOffset={1} xs={11} >fv</Col>
          </Row>
      </Grid>
    </div>
  );
}

export default App;
