import React from 'react'
import { Select } from 'semantic-ui-react'

const countryOptions = [
  { key: 'soft_skill_01', value: 'soft_skill_01', text: 'Sens de la pédagogie' },
  { key: 'soft_skill_02', value: 'soft_skill_02', text: 'Être rigoureux' },
  { key: 'soft_skill_03', value: 'soft_skill_03', text: 'Sens des affaires et de la valeur de l’argent' },
  { key: 'soft_skill_04', value: 'soft_skill_04', text: 'Sens de l’organisation' },
  { key: 'soft_skill_05', value: 'soft_skill_05', text: 'Être autonome' },
  { key: 'soft_skill_06', value: 'soft_skill_06', text: 'Être à l’écoute' },
  { key: 'soft_skill_07', value: 'soft_skill_07', text: 'Avoir l’esprit d’équipe' },
  { key: 'soft_skill_08', value: 'soft_skill_08', text: 'Sens des responsabilités' },
  { key: 'soft_skill_09', value: 'soft_skill_09', text: 'Sens de l’initiative' },
  { key: 'soft_skill_10', value: 'soft_skill_10', text: 'Sens de l’analyse' },
  { key: 'soft_skill_11', value: 'soft_skill_11', text: 'Faire preuve d’autorité' },
  { key: 'soft_skill_12', value: 'soft_skill_12', text: 'Curiosité intellectuelle' },
  { key: 'soft_skill_13', value: 'soft_skill_13', text: 'Sens critique' },
  { key: 'soft_skill_14', value: 'soft_skill_14', text: 'Esprit de synthèse' },
  { key: 'soft_skill_15', value: 'soft_skill_15', text: 'Sens de l’innovation/créativité' },
  { key: 'soft_skill_16', value: 'soft_skill_16', text: 'Etre persévérant' },
  { key: 'soft_skill_17', value: 'soft_skill_17', text: 'Capacité d’adaptation' },
  { key: 'soft_skill_18', value: 'soft_skill_18', text: 'Sens des relations humaines' },
  { key: 'soft_skill_19', value: 'soft_skill_19', text: 'Réactivité' },
  { key: 'soft_skill_20', value: 'soft_skill_20', text: 'Faire preuve de diplomatie' },
  { key: 'soft_skill_21', value: 'soft_skill_21', text: 'Faire preuve de discrétion' },
  { key: 'soft_skill_22', value: 'soft_skill_22', text: 'Orientation clients' },
  { key: 'soft_skill_23', value: 'soft_skill_23', text: 'Maîtrise de soi' },
 

]

const SelectExample = () => (
  <Select placeholder='Select your country' options={countryOptions} />
)

export default SelectExample