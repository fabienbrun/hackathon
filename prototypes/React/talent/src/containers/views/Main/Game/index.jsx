import React, { useState } from 'react'
import './style.css'
import Switch from 'react-ios-switch'
import { Button, Form, Grid, Header, Image, Message, Segment, Label, Icon, Modal } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

import Advanced from '../../../../examples/Advanced'
import Simple from '../../../../examples/Simple'

import myImage from '../../../../img/icon.png';
import profileImage from '../../../../img/profil.png';

function Game () {
  const [showAdvanced, setShowAdvanced] = useState(false)
  const [open, setOpen] = React.useState(false)

  return (
    <div className='app'>
       <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button>Show Modal</Button>}
      stye={{
        
      }}
    >
      
      <Modal.Content image onClick={() => setOpen(false)} >
        <Image size='medium' src={profileImage} wrapped />
       
      </Modal.Content>
     
    </Modal>
      <Button
      onClick={() => setOpen(!open)}
      style={{
        position: "absolute",
        top: "2%",
        left: "2%",
        backgroundColor:"transparent"
      }} icon>
         <Image src={myImage} 
          alt="Logo" 
          width="20%" 
          style={{
            marginLeft: "0",
            marginTop: "0"
         }}
    />  
      </Button>
      {showAdvanced ? <Advanced /> : <Simple />}
      {/* <div className='row'>
        <p style={{ color: '#fff' }}>Show advanced example</p> <Switch checked={showAdvanced} onChange={setShowAdvanced} />
      </div> */}
    </div>
  )
}

export default Game