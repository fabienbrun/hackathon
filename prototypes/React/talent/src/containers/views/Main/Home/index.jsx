import React from 'react'
import { Link } from 'react-router-dom'
import {Container, Button, Grid, Image, Label} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

import myImage from '../../../../img/logohome.png';



export default () => (
  <div style={{
      minHeight: '100vh', 
      display: 'flex', 
      flexFlow: 'column nowrap', 
      backgroundColor: 'white',
      backgroundImage: `url(${require("../../../../img/bg.png") } )`,
      backgroundSize: 'contain',
      backgroundRepeat: 'none'
    }}>

   
      <Label style={{
        backgroundColor: "white",
        borderRadius: "0px",
        width: "100%",
        textAlign: "center",
        padding: "5%",
        paddingLeft: "10%",
        paddingRight: "10%",
        fontSize: "1.5em",
        color: "#224893",
        borderBottomLeftRadius: "30px",
        borderBottomRightRadius: "30px",
        fontWeight: "200",
        }} >Hello, bienvenue sur ton révélateur de talent ! </Label>


    <img src={myImage} 
    alt="Logo" 
    width="80%" 
    style={{
      marginLeft: "10%",
      marginTop: "55%"
    }}
    />  

    <Link to='/form' style={{
      color: "white",
      backgroundColor: "#f9c101",
      width: "50%",
      height: "60px",
      padding: "10px",
      position: "absolute",
      textAlign: "center",
      top: "80%",
      left: "25%",
      borderRadius:"30px",
      fontSize: "1.5em",
      padding: "0px",
      margin: "0px",
      paddingTop: "20px"
      
    }}
    >Commencer</Link>
  

  
  
  </div>
)
