import React from 'react'
import { Link } from 'react-router-dom'
import { Button, Form, Grid, Header, Image, Message, Segment, Label } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

import personImage from '../../../../img/person.png';


export default () => (
  <div style={{
    minHeight: '100vh', 
    display: 'flex', 
    flexFlow: 'column nowrap', 
    backgroundColor: 'white',
    backgroundImage: `url(${require("../../../../img/bg.png") } )`,
    backgroundSize: 'contain',
    backgroundRepeat: 'none'
  }}>


  <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
    
  <Grid.Column style={{ maxWidth: "80%" }}>
  

  <img src={personImage} 
    alt="Logo" 
    width="25%" 
    style={{
      marginLeft: "0",
      marginTop: "0"
    }}
    />  



    <Header as='h2' color='teal' textAlign='center'>
      {/* Log-in to your account */}
    </Header>
    <Form size='large'>
      <Segment stacked>
        <Form.Input fluid icon='user' color="#1f3065" iconPosition='left' placeholder='Nom' />
        <Form.Input fluid icon='user' iconPosition='left' placeholder='Prenom' />
        <Form.Input fluid icon='mail' iconPosition='left' placeholder='Email' />
        <Form.Input fluid icon='lock' iconPosition='left' placeholder='Mot de pass' />
        <Label as='a' style={{
          width:"40%",
          backgroundColor:"#f8b914",
          marginBottom: "10px"
        }}>
          <Image avatar spaced='right' src='https://react.semantic-ui.com/images/avatar/small/elliot.jpg' />
          Garçon
        </Label>
        <Label as='a'  style={{
          width:"40%",
          marginBottom: "10px"
        }}
        >
          <img src='https://react.semantic-ui.com/images/avatar/small/stevie.jpg' style={{marginRight: "10px"}} />
          Fille
        </Label>
        
        <Form.Input
          fluid
          icon='lock'
          iconPosition='left'
          placeholder='Situation de famille'
          type='password'
          style={{
            marginBottom: "20px"
          }}
        />

        <Link to='/game' style={{
          padding:"10px",
          paddingLeft: "40%",
          paddingRight: "40%",
          backgroundColor: "#f8b914",
          color: "white",
        }} fluid size='large'>
          S'inscrire
        </Link>


      </Segment>
    </Form>
    <Message>
      Vous avez déjà un compte ? <a href='#'><br/> <p style={{color: "#1f3065"}}>Se connecter</p> </a>
    </Message>
  </Grid.Column>
</Grid>

</div>
)