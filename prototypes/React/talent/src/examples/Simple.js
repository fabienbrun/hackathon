import React, { useState } from 'react'
// import TinderCard from '../react-tinder-card/index'
import TinderCard from 'react-tinder-card'

const db = [
  {
    name: 'Fais un choix !',
    left: 'non',
    right: 'oui',
    url: './img/question1.png'
  },
  {
    name: 'Erlich Bachman',
    left: 'non',
    right: 'oui',
    url: './img/question2.png'
  },
  {
    name: 'Monica Hall',
    left: 'non',
    right: 'oui',
    url: './img/question3.png'
  },
  {
    name: 'Jared Dunn',
    left: 'non',
    right: 'oui',
    url: './img/question4.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question5.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question6.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question7.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question8.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question9.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question10.png'
  },
  {
    name: 'Dinesh Chugtai',
    left: 'non',
    right: 'oui',
    url: './img/question11.png'
  }
]

function Simple () {
  const characters = db
  const [lastDirection, setLastDirection] = useState()

  const swiped = (direction, nameToDelete) => {
   
    console.log('removing: ' + nameToDelete)
   
    setLastDirection(direction)
  }

  const outOfFrame = (name) => {
    console.log(name + ' left the screen!')
   
  }

  return (
    <div>
      <link href='https://fonts.googleapis.com/css?family=Damion&display=swap' rel='stylesheet' />
      <link href='https://fonts.googleapis.com/css?family=Alatsi&display=swap' rel='stylesheet' />
      {/* <h1>{character.name}</h1> */}

      <div className='cardContainer'>
        {characters.map((character, index) =>
        <div>
          {/* <h1 style={{
            position: "absolute",
            top: "0px",
            width: "100%",
            padding:"0",
            padding: "20px",
            left:0,
            margin:"0",
            textAlign: "center",
            backgroundColor:"#1f3065"
            }}>Tu préfères bricoler ton vélo ou jouer au basket ?</h1> */}
          <TinderCard className='swipe' key={character.name} onSwipe={(dir) => swiped(dir, character.name)} onCardLeftScreen={() => { console.log("Out !!!!!");   outOfFrame(character.name)}}>
            <div style={{ backgroundImage: 'url(' + character.url + ')' }} className='card'>
              {/* <h3>{character.name}</h3> */}
            </div>
          </TinderCard>
          </div>
        )}
      </div>
      {/* {lastDirection ? <h2 className='infoText'>You swiped {lastDirection}</h2> : <h2 className='infoText' />} */}
    </div>
  )
}

export default Simple
