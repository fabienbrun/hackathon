//Variables for setup

let container;
let camera;
let renderer;
let scene;
let house;
var raycaster, mouse = { x : 0, y : 0 };

function init() {
  container = document.querySelector(".scene");

  //Create scene
  scene = new THREE.Scene();

  const fov = 35;
  const aspect = container.clientWidth / container.clientHeight;
  const near = 0.1;
  const far = 1000;

  //Camera setup
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(0, 4, 34);
  

  const ambient = new THREE.AmbientLight(0x404040, 1);
  scene.add(ambient);

  const light = new THREE.DirectionalLight(0xffffff, 2);
  light.position.set(0, 0, 10);
  scene.add(light);


  //Renderer
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setSize(container.clientWidth, container.clientHeight);
  renderer.setPixelRatio(window.devicePixelRatio);

  container.appendChild(renderer.domElement);


  


  //Load Model
  let loader = new THREE.GLTFLoader();
  loader.load("./pyramid-competences2.gltf", function(gltf) {
    scene.add(gltf.scene);
    //house = gltf.scene.children[0];
    

    house = gltf.scene;
    house.position.x = 0;
    house.rotation.x = 0.2;


    var geometry1 = new THREE.BoxGeometry( 1.9, 1.5, 1.9 );
    var material1 = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    var cube1 = new THREE.Mesh( geometry1, material1 );
    cube1.name = "cube1";
    cube1.position.y = 4.5;
    house.add( cube1 );


    var geometry2 = new THREE.BoxGeometry( 3.8, 1.5, 1.9 );
    var material2 = new THREE.MeshBasicMaterial( { color: 0x02288c } );
    var cube2 = new THREE.Mesh( geometry2, material2 );
    cube2.name = "cube2";
    cube2.position.y = 3.1;
    cube2.position.z = 1;
    house.add( cube2 );


    var geometry3 = new THREE.BoxGeometry(  3.8, 1.5, 1.9);
    var material3 = new THREE.MeshBasicMaterial( { color: 0xd27d00 } );
    var cube3 = new THREE.Mesh( geometry3, material3 );
    cube3.name = "cube3";
    cube3.position.y = 3.1;
    cube3.position.z = -1;
    house.add( cube3 );

    var geometry4 = new THREE.BoxGeometry(  3.8, 1.4, 1.9 );
    var material4 = new THREE.MeshBasicMaterial( { color: 0xa82020 } );
    var cube4 = new THREE.Mesh( geometry4, material4 );
    cube4.name = "cube4";
    cube4.position.y = 1.7;
    cube4.position.z = -2;
    cube4.position.x = -1;
    house.add( cube4 );

    var geometry5 = new THREE.BoxGeometry(  1.9, 1.4, 3.8 );
    var material5 = new THREE.MeshBasicMaterial( { color: 0x08421e } );
    var cube5 = new THREE.Mesh( geometry5, material5 );
    cube5.name = "cube5";
    cube5.position.y = 1.7;
    cube5.position.z = -1.1;
    cube5.position.x = 2;
    house.add( cube5 );

    var geometry6 = new THREE.BoxGeometry(  3.8, 1.4, 1.9 );
    var material6 = new THREE.MeshBasicMaterial( { color: 0xd27d00 } );
    var cube6 = new THREE.Mesh( geometry6, material6 );
    cube6.name = "cube6";
    cube6.position.y = 1.7;
    cube6.position.z = 2;
    cube6.position.x = 1;
    house.add( cube6 );

    var geometry7 = new THREE.BoxGeometry(  1.9, 1.4, 3.8 );
    var material7 = new THREE.MeshBasicMaterial( { color: 0x370159 } );
    var cube7 = new THREE.Mesh( geometry7, material7 );
    cube7.name = "cube7";
    cube7.position.y = 1.7;
    cube7.position.z = 1;
    cube7.position.x = -2;
    house.add( cube7 );

    var geometry8 = new THREE.BoxGeometry(  3.8, 1.4, 1.9 );
    var material8 = new THREE.MeshBasicMaterial( { color: 0x02288c } );
    var cube8 = new THREE.Mesh( geometry8, material8 );
    cube8.name = "cube8";
    cube8.position.y = 0.2;
    cube8.position.z = -3;
    cube8.position.x = 2;
    house.add( cube8 );

    var geometry9 = new THREE.BoxGeometry(  3.8, 1.4, 1.9 );
    var material9 = new THREE.MeshBasicMaterial( { color: 0xd27d00 } );
    var cube9 = new THREE.Mesh( geometry9, material9 );
    cube9.name = "cube9";
    cube9.position.y = 0.2;
    cube9.position.z = -3;
    cube9.position.x = -2;
    house.add( cube9 );

    var geometry10 = new THREE.BoxGeometry(  3.8, 1.3, 1.9 );
    var material10 = new THREE.MeshBasicMaterial( { color: 0xd27d00 } );
    var cube10 = new THREE.Mesh( geometry10, material10 );
    cube10.name = "cube10";
    cube10.position.y = 0.3;
    cube10.position.z = 3;
    cube10.position.x = -1.9;
    house.add( cube10 );

    var geometry11 = new THREE.BoxGeometry(  3.8, 1.3, 1.9 );
    var material11 = new THREE.MeshBasicMaterial( { color: 0xab3802 } );
    var cube11 = new THREE.Mesh( geometry11, material11 );
    cube11.name = "cube11";
    cube11.position.y = 0.3;
    cube11.position.z = 3;
    cube11.position.x = 1.9;
    house.add( cube11 );

    var geometry12 = new THREE.BoxGeometry(  1.9, 1.3, 3.8 );
    var material12 = new THREE.MeshBasicMaterial( { color: 0x370159 } );
    var cube12 = new THREE.Mesh( geometry12, material12 );
    cube12.name = "cube12";
    cube12.position.y = 0.3;
    cube12.position.z = 0;
    cube12.position.x = 3;
    house.add( cube12 );

    var geometry13 = new THREE.BoxGeometry(  1.9, 1.3, 3.8 );
    var material13 = new THREE.MeshBasicMaterial( { color: 0x08421e } );
    var cube13 = new THREE.Mesh( geometry13, material13 );
    cube13.name = "cube13";
    cube13.position.y = 0.3;
    cube13.position.z = 0;
    cube13.position.x = -3;
    house.add( cube13 );
    


    animate();
  });

  
}

function animate() {
  requestAnimationFrame(animate);
  house.rotation.y += 0.005;
  renderer.render(scene, camera);

}


function raycast ( e ) {
  // Step 1: Detect light helper
      //1. sets the mouse position with a coordinate system where the center
      //   of the screen is the origin
      mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
  
      //2. set the picking ray from the camera position and mouse coordinates
      raycaster.setFromCamera( mouse, camera );    
  
      //3. compute intersections (note the 2nd parameter)
      var intersects = raycaster.intersectObjects( scene.children, true );
  
      for ( var i = 0; i < intersects.length; i++ ) {
          //console.log( intersects[ i ] ); 

          

         // console.log( intersects[0].object.name ); 

          if(intersects[i].object.name == "cube1"){
           console.log("Cube1 !!!!!!!");
           document.getElementsByClassName("title-container1")[0].style.opacity = 1;
           document.getElementsByClassName("title-container2")[0].style.opacity = 0;
           document.getElementsByClassName("title-container3")[0].style.opacity = 0;
           document.getElementsByClassName("title-container4")[0].style.opacity = 0;
           document.getElementsByClassName("title-container5")[0].style.opacity = 0;
           document.getElementsByClassName("title-container6")[0].style.opacity = 0;
           document.getElementsByClassName("title-container7")[0].style.opacity = 0;
           document.getElementsByClassName("title-container8")[0].style.opacity = 0;
           document.getElementsByClassName("title-container9")[0].style.opacity = 0;
           document.getElementsByClassName("title-container10")[0].style.opacity = 0;
           document.getElementsByClassName("title-container11")[0].style.opacity = 0;
           document.getElementsByClassName("title-container12")[0].style.opacity = 0;
           document.getElementsByClassName("title-container13")[0].style.opacity = 0;
           break;
          }
          if(intersects[i].object.name == "cube2"){
            console.log("Cube2 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 1;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube3"){
            console.log("Cube3 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 1;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube4"){
            console.log("Cube4 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 1;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube5"){
            console.log("Cube5 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 1;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube6"){
            console.log("Cube6 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 1;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube7"){
            console.log("Cube7 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 1;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube8"){
            console.log("Cube8 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 1;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube9"){
            console.log("Cube9 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 1;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube10"){
            console.log("Cube10 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 1;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube11"){
            console.log("Cube11 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 1;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube12"){
            console.log("Cube12 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 1;
            document.getElementsByClassName("title-container13")[0].style.opacity = 0;
            break;
           }
           if(intersects[i].object.name == "cube13"){
            console.log("Cube13 !!!!!!!");
            document.getElementsByClassName("title-container1")[0].style.opacity = 0;
            document.getElementsByClassName("title-container2")[0].style.opacity = 0;
            document.getElementsByClassName("title-container3")[0].style.opacity = 0;
            document.getElementsByClassName("title-container4")[0].style.opacity = 0;
            document.getElementsByClassName("title-container5")[0].style.opacity = 0;
            document.getElementsByClassName("title-container6")[0].style.opacity = 0;
            document.getElementsByClassName("title-container7")[0].style.opacity = 0;
            document.getElementsByClassName("title-container8")[0].style.opacity = 0;
            document.getElementsByClassName("title-container9")[0].style.opacity = 0;
            document.getElementsByClassName("title-container10")[0].style.opacity = 0;
            document.getElementsByClassName("title-container11")[0].style.opacity = 0;
            document.getElementsByClassName("title-container12")[0].style.opacity = 0;
            document.getElementsByClassName("title-container13")[0].style.opacity = 1;
            break;
           }
           

          /*
              An intersection has the following properties :
                  - object : intersected object (THREE.Mesh)
                  - distance : distance from camera to intersection (number)
                  - face : intersected face (THREE.Face3)
                  - faceIndex : intersected face index (number)
                  - point : intersection point (THREE.Vector3)
                  - uv : intersection point in the object's UV coordinates (THREE.Vector2)
          */
      }
  // Step 2: Detect normal objects
      //1. sets the mouse position with a coordinate system where the center
      //   of the screen is the origin
      mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
  
      //2. set the picking ray from the camera position and mouse coordinates
      raycaster.setFromCamera( mouse, camera );    
  
      //3. compute intersections (no 2nd parameter true anymore)
      var intersects = raycaster.intersectObjects( scene.children );
  
      for ( var i = 0; i < intersects.length; i++ ) {
        
         // console.log( intersects[ i ] ); 
          /*
              An intersection has the following properties :
                  - object : intersected object (THREE.Mesh)
                  - distance : distance from camera to intersection (number)
                  - face : intersected face (THREE.Face3)
                  - faceIndex : intersected face index (number)
                  - point : intersection point (THREE.Vector3)
                  - uv : intersection point in the object's UV coordinates (THREE.Vector2)
          */
      }
  }





init();

function onWindowResize() {
  camera.aspect = container.clientWidth / container.clientHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(container.clientWidth, container.clientHeight);

  raycaster = new THREE.Raycaster();
  renderer.domElement.addEventListener( 'click', raycast, false );

}

window.addEventListener("resize", onWindowResize);

setTimeout(function () {
  console.log("youuuuuuuuuu!");
  onWindowResize()
}, 1000);
